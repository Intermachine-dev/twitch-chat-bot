# TWITCH CHAT BOT

bot de codigo abierto para el realizar chatbots con nodejs para la realizacion de chat bot en twitch usando variables de etorno en el proyecto se debe contar con un archivo .env para desarrollo

## NPM modules:

* tmi.js
* dotenv
* nodemon *solo para desarrollo*

para el desarrollo  o aportar de bot se debe conocer nodejs, git, la terminal de sistema operativo, es6+ y variables de entorno

| comandos npm | descripción |
| ----------- | ----------- |
| npm i o npm install | instalar las dependencias |
| npm start | inicia el proyecto de node sin detectar cambios |
| npm run dev | ejecuta el nodemon detectar cambios sin salir |

ejemplos:

ejecución del script
![alt text](docs/ejemplo1.png "node in development mode")

desarrollando el codigo
![alt text](docs/ejemplo2.png "writing twitch chat bot")

> para desplegar el chat bot de twitch debes usar servidores dedicados o PaaS como ejemplo: *Heroku*

links: 

[twitch tmi token](https://twitchapps.com/tmi/)
[tmi.js github repo](https://github.com/tmijs/tmi.js)
[twitch developers portal](https://dev.twitch.tv/)

licencia __*MIT*__
